#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>


#define KIB 1024
#define MIB (1024 * 1024)


typedef uint64_t cpu_cycles_t;


static inline cpu_cycles_t rdtsc(void) {
    uint32_t hi, lo;
    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t) lo) | (((uint64_t) hi) << 32u);
}


#define TOTAL_ALLOCATED_ELEMENTS    (300 * MIB / (int) sizeof(int))
#define TOTAL_READ_ELEMENTS         TOTAL_ALLOCATED_ELEMENTS


static inline cpu_cycles_t walk_array(register const int *array) {
    if (NULL == array) {
        fprintf(stderr, "Array is NULL\n");
        return 0;
    }

    register int i;
    register int k;

    const int n_walks = 3;

    cpu_cycles_t volatile t1, t2;
    cpu_cycles_t t_min = UINT64_MAX;

    for (int j = 0; j < n_walks; j++) {
        t1 = rdtsc();

        for (k = 0, i = 0; i < TOTAL_READ_ELEMENTS; i++) {
            k = array[k];
        }

        t2 = rdtsc();

        printf("%d\r", (int) k);

        if (t_min > t2 - t1) {
            t_min = t2 - t1;
        }
    }

    return t_min / TOTAL_READ_ELEMENTS;
}

int *fill_array(
        int *array,
        const int n_parts,
        const int offset,
        const int size
) {
    if (NULL == array) {
        return NULL;
    }

    int i;
    int j = 1;

    for (i = 0; i < size / n_parts; i++) {
        for (j = 1; j < n_parts; j++) {
            array[i + (j - 1) * offset] = i + j * offset;
        }
        array[i + (j - 1) * offset] = i + 1;
    }

    array[i - 1 + (j - 1) * offset] = 0;

    return array;
}


#define NAME_OF(It) #It


void measure_read_time(
        int *array,
        const int min_fragments_count,
        const int max_fragments_count,
        const int cache_size_bytes,
        const int cache_associativity,
        cpu_cycles_t *results
) {
    for (int fragments_count = min_fragments_count; fragments_count <= max_fragments_count; fragments_count += 1) {
        const cpu_cycles_t read_time = walk_array(fill_array(
                array,
                fragments_count,
                cache_size_bytes / (int) sizeof(int) / cache_associativity,
                cache_size_bytes / (int) sizeof(int)
        ));
        results[fragments_count - min_fragments_count] = read_time;
        fprintf(
                stderr,
                NAME_OF(fragments_count) "=%d, " NAME_OF(read_time) "=%" PRIu64 "\n",
                fragments_count, read_time
        );
    }
}


#define L1_SIZE_BYTES (32 * KIB)
#define L2_SIZE_BYTES (256 * KIB)
#define L3_SIZE_BYTES (4 * MIB)

#define L1_ASSOCIATIVITY    8
#define L2_ASSOCIATIVITY    8
#define L3_ASSOCIATIVITY    16


#define MIN_FRAGMENTS_COUNT 1
#define MAX_FRAGMENTS_COUNT 32

#define TESTS_COUNT (MAX_FRAGMENTS_COUNT - MIN_FRAGMENTS_COUNT + 1)


int main(void) {
    int *array = (int *) calloc(TOTAL_ALLOCATED_ELEMENTS, sizeof(int));
    if (array == NULL) {
        perror("array");
        exit(EXIT_FAILURE);
    }

    cpu_cycles_t l1_times[TESTS_COUNT] = {0};
    cpu_cycles_t l2_times[TESTS_COUNT] = {0};
    cpu_cycles_t l3_times[TESTS_COUNT] = {0};

    measure_read_time(array, MIN_FRAGMENTS_COUNT, MAX_FRAGMENTS_COUNT, L1_SIZE_BYTES, L1_ASSOCIATIVITY, l1_times);
    measure_read_time(array, MIN_FRAGMENTS_COUNT, MAX_FRAGMENTS_COUNT, L2_SIZE_BYTES, L2_ASSOCIATIVITY, l2_times);
    measure_read_time(array, MIN_FRAGMENTS_COUNT, MAX_FRAGMENTS_COUNT, L3_SIZE_BYTES, L3_ASSOCIATIVITY, l3_times);

    printf("Fragments Count,L1 Access Time,L2 Access Time,L3 Access Time\n");
    for (int i = 0; i < TESTS_COUNT; i += 1) {
        printf(
                "%d,%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n"
                "", MIN_FRAGMENTS_COUNT + i, l1_times[i], l2_times[i], l3_times[i]
        );
    }

    free(array);
    exit(EXIT_SUCCESS);
}

